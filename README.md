
# Summary

Raw data behind www.extropicstudios.com

See https://gitlab.com/ExtropicStudios/build for usage.

# TODO

- [ ] Integrate Pinboard w/ cool links
- [ ] Rewrite cool links page layout to make better use of tags