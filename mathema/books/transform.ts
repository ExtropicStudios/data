
export = (data) => {

  ///////////////////
  // collect stats //
  ///////////////////

  var unique_authors = {};
  var yearCounts = {};
  data.oldestBook = undefined;
  data.finished = 0;
  data.unfinished = 0;

  data.books.forEach(function(b) {

    if (b.authors == undefined) {
      console.log("authors was not defined for ");
      console.log(b);
    }

    if (b.authors.length > 2) {
      b.authors = ["Various"];
    }

    b.displayYear = (b.year >= 0) ? b.year : (-b.year) + " BC";

    if (b.finished) {
      data.finished++;

      b.authors.forEach(function(a) {
        unique_authors[a] = true;
      });

      if (yearCounts[b.year] == undefined) {
        yearCounts[b.year] = 0;
      }
      yearCounts[b.year]++;

      if (data.oldestBook === undefined || b.year < data.oldestBook.year) {
        data.oldestBook = b;
      }

    } else {
      data.unfinished++;
    }
  });

  data.uniqueAuthors = Object.keys(unique_authors).length;

  let mostPopularYear = max(yearCounts);
  data.mostPopularYear = yearCounts[mostPopularYear] + " books read from " + mostPopularYear;

  //////////
  // Sort //
  //////////

  data.books = data.books.sort(function(a, b) {
    //console.log("comparing " + a.title + " and " + b.title);
    // Sort first by year
    if (b.year - a.year !== 0) {
      //console.log("  sorting on year: " + a.year + " and " + b.year);
      return b.year - a.year
    }

    // Sort by author last name
    // TODO: this could be improved by having separate first name/last name fields
    let b_lastName = b.authors[0].split(" ").slice(-1)[0].toLowerCase();
    let a_lastName = a.authors[0].split(" ").slice(-1)[0].toLowerCase();
    if (b_lastName < a_lastName) {
      //console.log("  sorting on author last name: " + a_lastName + " and " + b_lastName);
      return 1;
    } else if (b_lastName > a_lastName) {
      //console.log("  sorting on author last name: " + a_lastName + " and " + b_lastName);
      return -1;
    }

    // Sort by author first name
    let b_firstName = b.authors[0].split(" ")[0].toLowerCase();
    let a_firstName = a.authors[0].split(" ")[0].toLowerCase();
    if (b_firstName < a_firstName) {
      //console.log("  sorting on author first name: " + a_firstName + " and " + b_firstName);
      return 1;
    } else if (b_firstName > a_firstName) {
      //console.log("  sorting on author first name: " + a_firstName + " and " + b_firstName);
      return -1;
    }

    // Attempt to sort by series
    for (let s in b.series) {
      if (a.series && a.series[s]) {
        return b.series[s] - a.series[s];
      }
    }

    // Sort by title
    if (cleanTitle(b.title) < cleanTitle(a.title)) {
      return 1;
    } else if (cleanTitle(b.title) > cleanTitle(a.title)) {
      return -1;
    }

    return 0;
  });

  return data;
};

///////////////////////
// Utility functions //
///////////////////////

function max(obj) {
  var maxKey = undefined;
  var maxValue = 0;
  for (var k in obj) {
    if (obj[k] > maxValue) {
      maxValue = obj[k];
      maxKey = k;
    }
  }
  return maxKey;
}

// Strips words like "A", "The", etc from the beginning of titles for better
// sorting.
function cleanTitle(title) {
  return title
    .toLowerCase()
    // TODO: only trim from beginning of string
    //.replace("a", "")
    //.replace("the", "");
}
