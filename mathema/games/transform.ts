
export = (data) => {

  data.games = data.games.sort(function(a, b) {
    if (b.year - a.year !== 0) {
      return b.year - a.year
    }

    if (b.title < a.title) {
      return 1;
    }
    return -1;
  });

  return data;
}
