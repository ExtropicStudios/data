
export = (data) => {

  ///////////////
  // transform //
  ///////////////

  // Filter out strings so we can use them as comments
  data.movies = data.movies.filter(function(m) {
    if (typeof m == "string") {
      return false;
    }
    return true;
  });

  data.watched = 0;
  data.unwatched = 0;

  data.movies.forEach(function(m) {
    if (m.watched) {
      data.watched++;
    } else {
      data.unwatched++;
    }
  });

  //////////
  // sort //
  //////////

  data.movies = data.movies.sort(function(a, b) {
    if (b.year - a.year !== 0) {
      return b.year - a.year
    }

    if (b.name < a.name) {
      return 1;
    } else if (b.name > a.name) {
      return -1;
    }

    return 0;
  });

  return data;
};
