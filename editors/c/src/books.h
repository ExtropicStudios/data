#ifndef MATHEMA_BOOKS_H
#define MATHEMA_BOOKS_H

#include "parson/parson.h"

#include "globals.h"

///////////////////////////////////////////////////////////////////////////////
//
// Code for working with the JSON data structure used by the book library.
//

JSON_Object* get_book(int book_index) {
  JSON_Object* root_object = json_value_get_object(root);
  JSON_Array* books = json_object_get_array(root_object, "books");
  return json_array_get_object(books, book_index);
}

#endif
