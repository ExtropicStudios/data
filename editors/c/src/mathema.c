#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "parson/parson.h"
#include "strsplit/strsplit.h"
#include "trim/trim.h"

// Mathema libraries

#include "books.h" // Book data structs and functions

///////////////////////////////////////////////////////////////////////////////
// Common code
//

// Shared variables

#include "globals.h"

JSON_Value* root; // JSON data will be stored here.
int quit = 0;     // Set this to 1 to quit the event loop.

// Boolean.
// True if the file has been modified since the last save.
// Set to true any time you modify the JSON data referenced by `root`.
int dirty = 0;

// Legacy compatibility

#ifndef SIZE_MAX
#define SIZE_MAX 18446744073709551615
#endif

// Function prototypes

// Implemented in common code

char* get_author_string(int book_index, size_t length);
char* get_formatted_title(int book_index, size_t length);
void save();

// UI-specific implementations

void ui_init();
void ui_deinit();
void render();
void await_event();

///////////////////////////////////////////////////////////////////////////////
//
// Master TODO List
//
// - Search
// - Sort view by arbitrary field
// - Description field (?)
// - Unicode support
// - Include other mathema libraries besides books (tabbed UI?)
// - Logging to a file
// - Key to toggle between localized and translated fields
// - Move globals to context struct
//

int main(int argc, char* argv[]) {

  ui_init();

  // read file

  root = json_parse_file("data.json");

  if (json_value_get_type(root) == JSONError) {
    printf("failed to parse\n");
    return 0;
  }

  if (json_value_get_type(root) != JSONObject) {
    printf("expected a JSON object at the root\n");
    return 0;
  }

  while (quit == 0) {
    render();
    await_event();
  }

  json_value_free(root);
  ui_deinit();

  return 0;
}

void save() {
  JSON_Status status = json_serialize_to_file_pretty(root, "data.json");
  if (status == JSONSuccess) {
    dirty = 0;
  } else {
    fprintf(stderr, "Error saving JSON file.\n");
    quit = 1;
  }
}

// Returns a string containing all the authors for a book concatenated with
// commas, trimmed to the given length with a trailing ellipses if necessary.
// A length of -1 will never trim.
// You must call free() on the string after using it.
char* get_author_string(int book_index, size_t length) {
  JSON_Object* book = get_book(book_index);

  JSON_Array* authors = 0;
  if (json_object_has_value(book, "authors_translated")) {
    authors = json_object_get_array(book, "authors_translated");
  } else if (json_object_has_value(book, "authors")) {
    authors = json_object_get_array(book, "authors");
  }

  if (authors == 0) {
    // TODO: better logging
    //fprintf(stderr, "book %d missing authors tag\n", book_index);
    return calloc(1, 0);
  }

  size_t author_count = json_array_get_count(authors);

  if (author_count == 0) {
    //fprintf(stderr, "book %d has no authors\n", book_index);
    return calloc(1, 0);
  }

  size_t author_string_size = (author_count * 2) - 1;
  for (int i = 0; i < author_count; i++) {
    author_string_size += strlen(json_array_get_string(authors, i));
  }

  char* author_string = (char*) malloc(author_string_size * sizeof(char));
  author_string[0] = 0;

  for (int i = 0; i < author_count - 1; i++) {
    strcat(author_string, json_array_get_string(authors, i));
    strcat(author_string, ", ");
  }
  strcat(author_string, json_array_get_string(authors, author_count - 1));

  // TODO: optimize to do this without a temp string copy
  if (author_string_size > length) {
    author_string[length - 3] = 0;
    strcat(author_string, "...");
  }
  size_t result_size = fmax(author_string_size, length + 1);
  char* result = (char*) malloc(result_size * sizeof(char));
  result[0] = 0;
  strcpy(result, author_string);
  free(author_string);

  return result;
}

// Concatenates the title and subtitle with a colon, and trims to the given
// length using ellipses if necessary.
// You must call free() on the result after using it.
char* get_formatted_title(int book_index, size_t length) {
  JSON_Object* book = get_book(book_index);

  const char* title;
  const char* subtitle;

  // TODO: default to english until i get utf-8 working
  // could have a button to toggle between english and native translations
  if (json_object_has_value(book, "title_translated")) {
    title = json_object_get_string(book, "title_translated");
  } else {
    title = json_object_get_string(book, "title");
  }

  // TODO: add trailing ellipses to truncated titles
  if (json_object_has_value(book, "subtitle_translated")) {
    subtitle = json_object_get_string(book, "subtitle_translated");
  } else if (json_object_has_value(book, "subtitle")) {
    subtitle = json_object_get_string(book, "subtitle");
  } else {
    subtitle = 0;
  }

  size_t tmp_result_size = strlen(title) + (subtitle != 0 ? strlen(subtitle) : 0) + 3;
  char* tmp_result = (char*) malloc(tmp_result_size * sizeof(char));
  tmp_result[0] = 0;
  strcat(tmp_result, title);
  if (subtitle != 0) {
    strcat(tmp_result, ": ");
    strcat(tmp_result, subtitle);
  }

  // TODO: optimize to do this without a temp string copy
  if (tmp_result_size > length) {
    tmp_result[length - 3] = 0;
    strcat(tmp_result, "...");
  }
  size_t result_size = fmax(tmp_result_size, length + 1);
  char* result = (char*) malloc(result_size * sizeof(char));
  result[0] = 0;
  strcpy(result, tmp_result);
  free(tmp_result);

  return result;
}

// Returns the number of times c appears in str
int char_count(const char* str, char c) {
  int count = 0;
  for (int i = 0; str[i] != 0; i++) {
    if (str[i] == c) {
      count++;
    }
  }

  return count;
}

// Sort function to compare two books.
int compare_books(const void* a, const void* b) {
  JSON_Object* a_obj = json_value_get_object(*(JSON_Value**) a);
  JSON_Object* b_obj = json_value_get_object(*(JSON_Value**) b);

  int a_year = json_object_get_number(a_obj, "year");
  int b_year = json_object_get_number(b_obj, "year");

  if (a_year < b_year) {
    return 1;
  }
  if (a_year > b_year) {
    return -1;
  }

  // TODO: sort by first author's last name

  // TODO: ignore words like "the", "a", "an"
  const char* a_title = json_object_get_string(a_obj, "title");
  const char* b_title = json_object_get_string(b_obj, "title");

  return strcmp(a_title, b_title);
}

// This sorts the books within the JSON structure itself.
// The books are sorted by year. Sorting within the year is not yet defined.
void sort_books() {
  JSON_Object* root_obj = json_value_get_object(root);
  JSON_Array* initial = json_object_get_array(root_obj, "books");

  int count = json_array_get_count(initial);

  JSON_Value** vals_to_sort = malloc(count * sizeof(JSON_Value*));

  for (int i = 0; i < count; i++) {
    vals_to_sort[i] = json_value_deep_copy(json_array_get_value(initial, i));
  }

  json_object_remove(root_obj, "books");

  qsort(vals_to_sort, count, sizeof(JSON_Value*), compare_books);

  JSON_Array* sorted = json_value_get_array(json_value_init_array());
  for (int i = 0; i < count; i++) {
    json_array_append_value(sorted, vals_to_sort[i]);
  }

  free(vals_to_sort);

  json_object_set_value(root_obj, "books", json_array_get_wrapping_value(sorted));

  fprintf(stderr, "done doing!");
}

///////////////////////////////////////////////////////////////////////////////
// ncurses code
//

#if !defined(_WIN32)

#include <curses.h>
#include <form.h>

void ui_init() {
  initscr();
  keypad(stdscr, TRUE);
  nonl();
  cbreak();
  noecho();

  if (has_colors()) {
    start_color();

    // TODO: use init_color(COLOR_RED, 700, 0, 0); to tweak rgb values for these (scales from 0 to 1000)
    init_pair(1, COLOR_RED,     COLOR_BLACK);
    init_pair(2, COLOR_GREEN,   COLOR_BLACK);
    init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
    init_pair(4, COLOR_BLUE,    COLOR_BLACK);
    init_pair(5, COLOR_CYAN,    COLOR_BLACK);
    init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(7, COLOR_WHITE,   COLOR_BLACK);
  }
}

void ui_deinit() {
  endwin();
}

// Keeps track of scrolling through the list of books.
int list_offset = 0;
int book_selected = 0;
size_t book_count = 0;

const int STATUSBAR_HEIGHT = 3;

void render() {

  // TODO: render inside a scrollable window

  // clear the screen
  erase();
  // TODO: use solid color blocks instead
  box(stdscr, 0, 0);

  JSON_Object* root_object = json_value_get_object(root);
  JSON_Array* books = json_object_get_array(root_object, "books");
  book_count = json_array_get_count(books);

  int win_y, win_x, win_h, win_w;
  getmaxyx(stdscr, win_h, win_w);
  // account for borders
  win_h -= (STATUSBAR_HEIGHT + 2);
  win_y = 1;
  win_x = 1;
  win_w -= 2;

  int column_width = (win_w - 11) / 2;

  // scroll the window to show the selected book
  list_offset = fmin(book_selected, list_offset);
  list_offset = fmax(book_selected - (win_h-2), list_offset);
  // TODO: use solid color blocks instead
  mvhline(win_h, win_x, 0, win_w);
  mvprintw(win_h + STATUSBAR_HEIGHT + 1, win_w - 2, "[");
  if (dirty == 1) {
    attron(COLOR_PAIR(5));
    printw("*");
    attroff(COLOR_PAIR(5));
  } else {
    printw(" ");
  }
  printw("]");
  mvprintw(win_h + 1, win_x, " (/) Search - So(R)t - (S)ave - (M)ark Read - (E)dit - (A)dd - (Q)uit");
  mvprintw(win_h + 3, win_x, " Total books: %d", book_count);

  for (size_t i = list_offset; i < book_count; i++) {
    int cur_y = (i - list_offset) + win_y;
    if (cur_y >= win_h) {
      break;
    }

    if (i == book_selected) {
      attron(A_REVERSE);
    }

    char* title = get_formatted_title(i, column_width - 2);
    mvprintw(cur_y, win_x, "%s", title);
    free(title);

    char* author_string = get_author_string(i, column_width - 2);
    mvprintw(cur_y, column_width + 1, "%s", author_string);
    free(author_string);

    JSON_Object* book = json_array_get_object(books, i);

    JSON_Value* year = json_object_get_value(book, "year");
    if (json_value_get_type(year) == JSONNull) {
      mvprintw(cur_y, (column_width * 2) + 1, "(????)", year);
    } else {
      mvprintw(cur_y, (column_width * 2) + 1, "(%.0f)", json_value_get_number(year));
    }

    int finished = json_object_get_boolean(book, "finished");
    if (finished == 1) {
      printw(" [");
      attron(COLOR_PAIR(2));
      printw("X");
      attroff(COLOR_PAIR(2));
      printw("]");
    } else {
      printw(" [ ]");
    }

    if (json_object_has_value(book, "read_on")) {
      JSON_Array* read_on_dates = json_object_get_array(book, "read_on");
      // TODO: iterate through read on dates
    }

    if (json_object_has_value(book, "series")) {
      // TODO: figure out whether i want to keep this property
    }

    if (json_object_has_value(book, "tags")) {
      // TODO: figure out whether i want to keep this property
    }

    if (json_object_has_value(book, "translators")) {
      // TODO: move translators out of edition, just add duplicate entries for each edition
    }

    // TODO: add an "open in browser button?"
    const char* link = json_object_get_string(book, "link");
    //printw("  Link: %s\n", link);

    if (i == book_selected) {
      attroff(A_REVERSE);
    }
  }

  refresh();
}

void toggle_read() {
  JSON_Object* book = get_book(book_selected);
  int finished = json_object_get_boolean(book, "finished");
  if (finished == 1) {
    json_object_set_boolean(book, "finished", 0);
  } else {
    json_object_set_boolean(book, "finished", 1);
  }
  dirty = 1;
}

// Pops open an edit dialog for the given book.
// If book_index is -1, adds a new book instead.
void edit(int book_index) {

  int scr_h, scr_w;
  getmaxyx(stdscr, scr_h, scr_w);
  WINDOW* win = newwin(scr_h - (scr_h / 10), scr_w - (scr_w / 10), scr_h / 20, scr_w / 20);

  FIELD* fields[7];

  fields[0] = new_field(1, 100-7,  2,  9, 0, 0); // title
  fields[1] = new_field(1, 100-18, 4, 20, 0, 0); // title_translated
  fields[2] = new_field(1, 100-10, 6, 12, 0, 0); // subtitle
  fields[3] = new_field(1, 100-9,  8, 11, 0, 0); // authors (?)
  fields[4] = new_field(1, 100-6,  10, 8, 0, 0); // year
  fields[5] = new_field(1, 100-6,  12, 8, 0, 0); // link
  // TODO: add checkbox for finished status
  // TODO: add buttons for save / cancel
  fields[6] = 0;

  set_field_back(fields[0], A_UNDERLINE);
  set_field_back(fields[1], A_UNDERLINE);
  set_field_back(fields[2], A_UNDERLINE);
  set_field_back(fields[3], A_UNDERLINE);
  set_field_back(fields[4], A_UNDERLINE);
  set_field_back(fields[5], A_UNDERLINE);

  field_opts_off(fields[0], O_AUTOSKIP);
  field_opts_off(fields[1], O_AUTOSKIP);
  field_opts_off(fields[2], O_AUTOSKIP);
  field_opts_off(fields[3], O_AUTOSKIP | O_STATIC);
  field_opts_off(fields[4], O_AUTOSKIP);
  field_opts_off(fields[5], O_AUTOSKIP);

  if (book_index != -1) {
    JSON_Object* book = get_book(book_selected);

    set_field_buffer(fields[0], 0, json_object_get_string(book, "title"));
    set_field_buffer(fields[1], 0, json_object_get_string(book, "title_translated"));
    set_field_buffer(fields[2], 0, json_object_get_string(book, "subtitle"));

    char* author_string = get_author_string(book_selected, SIZE_MAX);
    set_field_buffer(fields[3], 0, author_string);
    free(author_string);

    char year[5];
    if (json_value_get_type(json_object_get_value(book, "year")) == JSONNull) {
      year[0] = '\0';
    } else {
      sprintf(year, "%4.0f", json_object_get_number(book, "year"));
    }
    set_field_buffer(fields[4], 0, year);
    set_field_buffer(fields[5], 0, json_object_get_string(book, "link"));
  }

  FORM* form = new_form(fields);

  set_form_win(form, win);

  post_form(form);

  mvwprintw(win, 2,  2, "Title: ");
  mvwprintw(win, 4,  2, "Translated Title: ");
  mvwprintw(win, 6,  2, "Subtitle: ");
  mvwprintw(win, 8,  2, "Authors: ");
  mvwprintw(win, 10, 2, "Year: ");
  mvwprintw(win, 12, 2, "Link: ");

  int win_h = getmaxy(win);
  mvwprintw(win, win_h - 2, 2, "(Enter): Save - (Esc): Cancel");

  form_driver(form, REQ_FIRST_FIELD);
  form_driver(form, REQ_END_LINE);

  box(win, 0, 0);
  wrefresh(win);

  int ch;
  while((ch = getch())) {
    if (ch == KEY_DOWN) {
      form_driver(form, REQ_NEXT_FIELD);
      form_driver(form, REQ_END_LINE);
    } else if (ch == KEY_UP) {
      form_driver(form, REQ_PREV_FIELD);
      form_driver(form, REQ_END_LINE);
    } else if (ch == KEY_LEFT) {
      form_driver(form, REQ_LEFT_CHAR);
    } else if (ch == KEY_RIGHT) {
      form_driver(form, REQ_RIGHT_CHAR);
    } else if (ch == 27) { // escape
      break;
    } else if (ch == 13) { // enter

      // go to the next field to trigger an update on field buffer contents
      form_driver(form, REQ_NEXT_FIELD);

      JSON_Object* root_object = json_value_get_object(root);
      JSON_Array* books = json_object_get_array(root_object, "books");
      JSON_Object* book;

      if (book_index == -1) {
        JSON_Value* book_val = json_value_init_object();
        book = json_value_get_object(book_val);
        json_array_append_value(books, book_val);
      } else {
        book = json_array_get_object(books, book_selected);
      }

      json_object_set_string(book, "title", trim(field_buffer(fields[0], 0)));
      const char* title_translated = trim(field_buffer(fields[1], 0));
      if (strlen(title_translated) > 0) {
        json_object_set_string(book, "title_translated", title_translated);
      }
      const char* subtitle = trim(field_buffer(fields[2], 0));
      if (strlen(subtitle) > 0) {
        json_object_set_string(book, "subtitle", subtitle);
      }

      const char* authors = trim(field_buffer(fields[3], 0));

      int commas = char_count(authors, ',') + 1;
      char** parts = malloc(commas * sizeof(char*));
      strsplit(authors, parts, ",");

      // TODO: for bonus points, sort authors alphabetically by last name
      json_object_set_value(book, "authors", json_value_init_array());
      JSON_Array* author_arr = json_object_get_array(book, "authors");
      for (int i = 0; i < commas; i++) {
        json_array_append_string(author_arr, trim(parts[i]));
      }
      free(parts);

      const char* year = trim(field_buffer(fields[4], 0));
      if (strcmp(year, "") == 0) {
        json_object_set_null(book, "year");
      } else {
        json_object_set_number(book, "year", atoi(field_buffer(fields[4], 0)));
      }
      const char* link = trim(field_buffer(fields[5], 0));
      if (strlen(link) > 0) {
        json_object_set_string(book, "link", link);
      }

      sort_books();

      dirty = 1;
      break;
    } else if (ch == 127) { // backspace
      form_driver(form, REQ_DEL_PREV);
    } else {
      // If this is a normal character, it gets typed in the field
      form_driver(form, ch);
    }
    box(win, 0, 0);
    wrefresh(win);
  }

  delwin(win);
  unpost_form(form);
  free_form(form);
  free_field(fields[0]);
  free_field(fields[1]);
  free_field(fields[2]);
  free_field(fields[3]);
  free_field(fields[4]);
  free_field(fields[5]);
}

void sort_mode() {
  // TODO
}

void search_mode() {
  char search_buf[50];
  search_buf[0] = 0;
  int bufindex = 0;

  int ch;
  while ((ch = getch())) {
    if (bufindex < 49 && (ch >= 'a' || ch <= 'z')) {
      search_buf[bufindex] = ch;
      search_buf[bufindex+1] = 0;
      bufindex++;
      // TODO: find the next book in the booklist that contains search_buf
      for (size_t i = book_selected; i < book_count; i++) {
        JSON_Object* book = get_book(i);
        // TODO: check title, author fields
      }
      // TODO: loop back around if not found before the end

    } else if (ch == 27) { // escape
      // TODO: jump back to the book you were looking at before you started searching
      return;
    } else if (ch == 13) { // enter
      // TODO: quit search mode, leave the pointer at the current searched book
      return;
    }
    render();
    // TODO: highlight the currently searched letters on top of the render() call
  }
}

void await_event() {
  int c = getch();
  if (c == 'q' || c == 27) {
    quit = 1;
  } else if (c == 's') {
    save();
  } else if (c == 'j' || c == KEY_DOWN) {
    book_selected = fmin(book_selected+1, book_count-1);
  } else if (c == 'k' || c == KEY_UP) {
    book_selected = fmax(book_selected-1, 0);
  } else if (c == 'm' || c == 'x') {
    toggle_read();
  } else if (c == 'e' || c == 13) { // enter key to edit
    edit(book_selected);
  } else if (c == 'a') {
    edit(-1);
  } else if (c == 'r') {
    sort_mode();
  } else if (c == '/') {
    search_mode();
  }
}

#endif

///////////////////////////////////////////////////////////////////////////////
// TODO: win32 code
//

#if defined(_WIN32)

void ui_init() {

}

void ui_deinit() {

}

void render() {

}

void await_event() {

}

#endif
