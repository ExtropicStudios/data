#ifndef MATHEMA_GLOBALS_H
#define MATHEMA_GLOBALS_H

#include "parson/parson.h"

// TODO: move these into a context struct that is passed around

extern JSON_Value* root; // JSON data will be stored here.
extern int quit;         // Set this to 1 to quit the event loop.
extern int dirty;        // Set this to 1 when you modify the JSON data.

#endif
