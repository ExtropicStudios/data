# TODO

## all

- [ ] win32 api interface
- [ ] x11 interface?
- [ ] parse games
- [ ] parse movies
- [ ] parse podcasts
- [ ] parse tv
- [ ] parse cool links
- [ ] parse lines

## ncurses

- [ ] switch to ncursesw for propert utf-8 display

## books

- [ ] fix search functionality
- [ ] basic dupe detection
- [ ] link book data up to worldcat account?

# DONE

- [X] ncurses interface
